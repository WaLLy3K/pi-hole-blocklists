# Pi-Hole blocklist

This blocklist was created by merging several existing blocklists into one. All files used are listed in `blocklist-files-urls.txt`. The final blocklist file is `blocklist.txt`.

**Add the list to your Pi-Hole**

* [Blocklist file: raw link](https://codeberg.org/Jeybe/pi-hole-blocklists/raw/branch/master/blocklist.txt)


## What's on the list?

In short: Domains that deliver **tracker and ads**.
The domains are collected and merged into one file by my [Pi-Hole merger script](https://codeberg.org/Jeybe/bash-scripts/src/branch/master/scripts/ph-merger).

**The goal** of this list is to make it possible to identify and block unwanted and harmful items in the internet. Effectively the domains on this list are known to deliver exactly these unwanted and harmful items, such as tracking and analytic scripts. As ads are often loaded from third party providers which are spying on users too, the domains from such providers are also included in this list.

## Which blocklists were unified?

| Name | Category | Project overview | RAW link | Licence |
| ---- | ----------- | ------- | -------- | ------- |
| add.207Net | Analytics | [Repository](https://github.com/FadeMind/hosts.extras) | [RAW file (unknows if official or not)](https://raw.githubusercontent.com/FadeMind/hosts.extras/master/add.2o7Net/hosts) | GPLv3 |
| Disconnect ad filter list | Ads and analytics | [Website](https://disconnect.me) | [RAW file (unknown if official or not)](https://s3.amazonaws.com/lists.disconnect.me/simple_ad.txt) | GPLv3 |
| EasyList | Ads | [Website](https://easylist.to/) | [RAW file (unofficial)](https://v.firebog.net/hosts/Easylist.txt) | GPLv3 / CC BY-SA 3.0 |
| EasyPrivacy | Analytics | [Website](https://easylist.to/) | [RAW file (unofficial)](https://v.firebog.net/hosts/Easyprivacy.txt) | GPLv3 / CC BY-SA 3.0 |
| NoTrack Tracker blocklist | Analytics | [Repository](https://gitlab.com/quidsup/notrack-blocklists) | [RAW file (official)](https://gitlab.com/quidsup/notrack-blocklists/raw/master/notrack-blocklist.txt) | GPLv3 |
| AnudeepND blacklist | Ads and analytics | [Repository](https://github.com/anudeepND/blacklist) | [RAW file (official)](https://raw.githubusercontent.com/anudeepND/blacklist/master/adservers.txt) | MIT Licence |
| Perflyst PiHoleBlocklist android tracking | Analytics | [Repository](https://github.com/Perflyst/PiHoleBlocklist/) | [RAW file (official)](https://raw.githubusercontent.com/Perflyst/PiHoleBlocklist/master/android-tracking.txt) | MIT Licence |
| Perflyst PiHoleBlocklist SmartTV | analytics | [Repository](https://github.com/Perflyst/PiHoleBlocklist/) | [RAW file (official)](https://raw.githubusercontent.com/Perflyst/PiHoleBlocklist/master/SmartTV.txt) | MIT Licence |
| WindowsSpyBlocker Spy Blocklist | analytics | [Repository](https://github.com/crazy-max/WindowsSpyBlocker) | [RAW file (official)](https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/spy.txt) | MIT Licence |
| AdAway default blocklist | Ads and analytics | [Website](https://adaway.org) | [RAW file (official)](https://adaway.org/hosts.txt) | CC Attribution 3.0 |
| Squidblacklist dg-ads | Ads | [Website](https://www.squidblacklist.org/) | [RAW file (official)](https://www.squidblacklist.org/downloads/dg-ads.acl) | "You may freely use, copy, and redistribute this blacklist in any manner you like." |
| hpHosts | Ads and Analytics | [Website](https://hosts-file.net) | [RAW file (official)](https://hosts-file.net/ad_servers.txt) | "This service is free to use, however, any and ALL automated use is strictly forbidden without express permission from ourselves" |
| AdGuard DNS | Ads and analytics | [Website](https://adguard.com) | [RAW file (unofficial)](https://v.firebog.net/hosts/AdguardDNS.txt) | unknown |
| Airelle trc | Ads | unknown | [RAW file (unknown if official or not)](https://v.firebog.net/hosts/Airelle-trc.txt) | unknown |
| Prigent Ads | Ads | unknown | [RAW file (unknown if official or not)](https://v.firebog.net/hosts/Prigent-Ads.txt) | unknown |
| UncheckyAds | Ads | [Repository](https://github.com/FadeMind/hosts.extras) | [RAW file (unknown if official or not)](https://raw.githubusercontent.com/FadeMind/hosts.extras/master/UncheckyAds/hosts) | unknown |

## Licence
The whole repository and the files in it are licenced under the **GPLv3**. A copy can be found [here](LICENCE).

